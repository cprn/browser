# browser
Simple wrapper for python bindings to Selenium web driver.

It takes care of some cumbersome tasks like:
* lazy-loading the driver when needed
* opening new windows
* keeping track of the windows queue
* maintaining focus on the last opened window
* closing windows
* unloading the driver when the last window gets closed

Running tests:
```sh
$ py.test
```

# todo
* proxy support & config
