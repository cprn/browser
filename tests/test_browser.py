import time
from socket import error as socket_error
from browser.browser import Browser


def count_windows(browser):
    try:
        return len(browser.driver.window_handles)
    except (AttributeError, socket_error):
        return 0


def test_open():
    for driver in ['Firefox', 'PhantomJS']:
        b = Browser(driver)
        b.open('http://example.com')
        assert b.driver.title == 'Example Domain'
        b.driver.quit()

def test_open_new_window():
    for driver in ['Firefox', 'PhantomJS']:
        assert count_windows(None) == 0
        b = Browser(driver)
        assert count_windows(b) == 0
        b.open('http://example.com')
        assert count_windows(b) == 1
        b.open('http://example.com')
        assert count_windows(b) == 2
        b.driver.quit()

def test_close():
    for driver in ['Firefox', 'PhantomJS']:
        b = Browser(driver)
        assert count_windows(b) == 0
        b.open('http://example.com')
        b.open('http://example.com')
        assert count_windows(b) == 2
        b.close()
        assert count_windows(b) == 1
        b.close()
        time.sleep(0.6) # closing last window takes ~600ms ???
        assert count_windows(b) == 0
        b.driver.quit()

def test_autofocus_last_window():
    for driver in ['Firefox', 'PhantomJS']:
        b = Browser(driver)
        b.open('http://example.com')
        first = b.driver.current_window_handle
        b.open('http://example.com')
        second = b.driver.current_window_handle
        b.open('http://example.com')
        third = b.driver.current_window_handle
        assert third != first
        b.close()
        assert b.driver.current_window_handle == second
        b.close()
        assert b.driver.current_window_handle == first
        b.driver.quit()
