import os
from socket import error as socket_error
from selenium import webdriver


class Browser:

    def __init__(self, type):
        self.type = type
        self.driver = None

    def open(self, url):
        try:
            self.driver.execute_script('window.open("%s")' % url)
            self.driver.switch_to_window(self.driver.window_handles[-1])
        except (AttributeError, socket_error):
            if self.type == 'Firefox':
                self.driver = webdriver.Firefox()
            if self.type == 'PhantomJS':
                self.driver = webdriver.PhantomJS()
            self.driver.get(url)

    def close(self):
        try:
            self.driver.close()
            self.driver.switch_to.window(self.driver.window_handles[-1])
        except:
            pass
